package com.example.calendarquickstart;

import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;

// This class is the receiver for the phone's SendDataMapToDataLayer service
// This class handles the receiving of the list of calendar events
// for displaying on the watch
public class MyListenerService extends WearableListenerService {

    public static final String TAG = "TAG";
    public static final String WEARABLE_DATA_PATH = "/wearable/data/path";
    public static ArrayList<String> phoneNumbers;
    public static ArrayList<String> eventTitles;
    public static ArrayList<String> eventTimes;

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        DataMap dataMap;
        for(DataEvent dataEvent:dataEvents){
            if(dataEvent.getType()==DataEvent.TYPE_CHANGED){
                String path = dataEvent.getDataItem().getUri().getPath();
                if(path.equalsIgnoreCase(WEARABLE_DATA_PATH)){
                    dataMap = DataMapItem.fromDataItem(dataEvent.getDataItem()).getDataMap().getDataMap("dataMap");
                    Log.v(TAG, "DataMap received on Wearable Device" + dataMap);

                    //Intent startIntent = new Intent(this,MainActivity.class);
                    //startIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    //times = dataMap.getLongArray("eventTimes");
                    phoneNumbers = dataMap.getStringArrayList("eventPhoneNumbers");
                    eventTitles = dataMap.getStringArrayList("eventTitles");
                   // eventTimes = dataMap.getStringArrayList("eventTimes");
                    //startIntent.putExtra("dataMap",myArray);
                    //startActivity(startIntent);

                    // TODO
                    // Possibly start an activity here, eventually,
                    // that notifies the user that a calendar update has occured.
                    // That might be too much though, not really sure.  Will
                    // need to discuss this with management to determine desired
                    // application behavior
                }
            }
        }
    }
}