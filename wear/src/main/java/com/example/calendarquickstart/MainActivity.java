package com.example.calendarquickstart;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

// This class is for the Android Watch
public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, WearableListView.ClickListener
{
    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if(googleApiClient!=null && googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    // Sample dataset for the list
    // Sample data is just being used for purposes of making sure
    // the list view works.
    String[] elements = {"List Item 1", "List Item 2"};
    GoogleApiClient googleApiClient;
    public static final String WEARABLE_DATA_PATH = "/wearable/data/path";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                 .addConnectionCallbacks(this)
                 .addOnConnectionFailedListener(this)
                .build();

        // Get the list component from the layout of the activity
        WearableListView listView =
                (WearableListView) findViewById(R.id.wearable_list);

        // Assign an adapter to the list
        listView.setAdapter(new Adapter(this, elements));

        // Set a click listener
        listView.setClickListener(this);
    }

    // WearableListView click listener
    @Override
    public void onClick(WearableListView.ViewHolder v)
    {
        Integer tag = (Integer) v.itemView.getTag();
        // use this data to complete some action ...
        Log.e("SUPERMAN", "" + tag);

        // Get the list component from the layout of the activity
        WearableListView listView =
                (WearableListView) findViewById(R.id.wearable_list);

        String listItem3 = "";
        String listItem4 = "";
        String listItem5 = "";

        int quantityOfCalendarEvents =0;
        if(MyListenerService.phoneNumbers !=null) {
            quantityOfCalendarEvents = MyListenerService.phoneNumbers.size();
            listItem3 = MyListenerService.phoneNumbers.get(0);
            Log.v("TAGTHAT", MyListenerService.phoneNumbers.get(0));
        }
        if(MyListenerService.eventTitles !=null)
            listItem4 = (String)MyListenerService.eventTitles.get(0);
        if(MyListenerService.eventTimes !=null)
            listItem5 = (String)MyListenerService.eventTimes.get(0);

        if(quantityOfCalendarEvents!=0){
            String[] newArray = {"List Item 1", "List Item 2", listItem3,listItem4,listItem5};
            listView.setAdapter(new Adapter(this, newArray));
        }
        else
        {
            String[] newArray = {"List Item 1", "List Item 2", listItem3,listItem4,listItem5};
            listView.setAdapter(new Adapter(this, newArray));

        }

        // Set a click listener
        listView.setClickListener(this);

        // Make the phone call
        sendMessage("18002655328"); // 1 800 COLLECT
    }

    @Override
    public void onTopEmptyRegionClick()
    {
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    // Eventually we would pass into this function, the actual phone
    // number associated with the Google Calendar event,
    // for sending.
    public void sendMessage(String phoneNumber){
        if(googleApiClient.isConnected()) {
            String phoneNumberOfInterest = phoneNumber;
            if (phoneNumberOfInterest == null || phoneNumberOfInterest.equals('')) {
                phoneNumberOfInterest = "18002655328"; // 1800 COLLECT
            }
            new SendMessageToDataLayer(WEARABLE_DATA_PATH, phoneNumberOfInterest, googleApiClient).start();
            // This sends phone number from Watch --> Phone
        }
    }
}