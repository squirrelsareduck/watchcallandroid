package com.example.calendarquickstart;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

// This class is in charge of sending/synchronizing the phone call information
// via the MessageAPI, to the phone.
public class SendMessageToDataLayer extends Thread {
    String path;
    String message;
    GoogleApiClient googleApiClient;
    String TAG = "tag";

    public SendMessageToDataLayer(String path, String message, GoogleApiClient googleApiClient) {
        this.path = path;
        this.message = message;
        this.googleApiClient = googleApiClient;
    }

    @Override
    public void run() {
        NodeApi.GetConnectedNodesResult nodesList = Wearable.NodeApi.getConnectedNodes(googleApiClient).await();
        for (Node node : nodesList.getNodes()) {
            MessageApi.SendMessageResult messageResult = Wearable.MessageApi.sendMessage(googleApiClient, node.getId(), path, message.getBytes()).await();
            if (messageResult.getStatus().isSuccess()) {
                //print success log
                Log.v(TAG, "Message: Successfully sent to" + node.getDisplayName());
                Log.v(TAG, "Message: Node Id is" + node.getId());
                Log.v(TAG, "Message: Node size is" + nodesList.getNodes().size());
            } else {
                // print failure log
                Log.v(TAG, "Error while sending Message");
            }
        }

    }
}
