# README #

This app is written as a part of my directed independent study at Florida Atlantic University, under the guidance of professor Hari Kalva.

This app has two main components: one part that gets installed on an Android phone, and the other part that gets installed on an Android Wear device (particularly with an Android Wear watch, as per design).  

To install on your device, connect your devices and install any USB drivers as necessary to sideload Android apps to your devices from your computer.  Load the code into Android studio.  Select "mobile" from the dropdown menu, then press the Run (Play) button.  This will load the phone's components.  Then select "wear" and press the Run (Play) button again.  This will load the wearable device's code components.

Open the app on your phone.  Right now, this just consists of a list of the data events: Title and time, followed by a button that sends the list to the wearable.

Open the app on your wearable.  Right now, this should have a list of the Google Calendar events, listed in order.  If you click on an item, and you had typed in a phone number into the event's "description", then your wearable device will send a message via bluetooth to your phone, signalling a call to be made to that phone number.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact