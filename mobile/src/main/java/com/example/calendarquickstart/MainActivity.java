package com.example.calendarquickstart;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;

import com.google.api.services.calendar.CalendarScopes;
import com.google.api.client.util.DateTime;

import com.google.api.services.calendar.model.*;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Data will be synchronized using DataItems and DataMaps.
//      Sent from Mobile
//      Received by Wearable
//          DataItem saved in Service until mobile
//          Activity is started, upon which data will be loaded.
// Phone calls will be made using MessageAPI
//      Sent from Mobile
//      Received by Wearable.

// Right now, this app just consists of a list of the data events with format:
// Event Title, Event Description, and Time,
// followed by a button that manually sends the list to the wearable.
// The automatic sending of the data should also occur, but this button is there
// for testing purposes.

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener
{
    GoogleAccountCredential mCredential;
    private TextView mOutputText;
    ProgressDialog mProgress;
    GoogleApiClient googleClient;
    public static ArrayList<String> eventPhoneNumbers;
    long[] eventTimes;
    public static ArrayList<String> eventTitles;
    public static ArrayList<String> eventTimesStrings;


    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = { CalendarScopes.CALENDAR_READONLY };
    public static final String WEARABLE_DATA_PATH = "/wearable/data/path";

    /**
     * Create the main activity.
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Right now, we just create a very elementary linearlayout
        // for the phone, since the focus of this application
        // is on the AndroidWear aspects.  Right now, this
        // phone app just has a button to synchronize the data from the calendar.
        // with the Watch.
        LinearLayout activityLayout = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        activityLayout.setLayoutParams(lp);
        activityLayout.setOrientation(LinearLayout.VERTICAL);
        activityLayout.setPadding(16, 16, 16, 16);

        ViewGroup.LayoutParams tlp = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        mOutputText = new TextView(this);
        mOutputText.setLayoutParams(tlp);
        mOutputText.setPadding(16, 16, 16, 16);
        mOutputText.setVerticalScrollBarEnabled(true);
        mOutputText.setMovementMethod(new ScrollingMovementMethod());
        activityLayout.addView(mOutputText);
        Button sendButton = new Button(this);
        sendButton.setText("SEND BUTTON");
        sendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = "Hello wearable\n Via the data layer";
                //Requires a new thread to avoid blocking the UI
                sendDataMapToDataLayer();
            }
        });
        activityLayout.addView(sendButton);

        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Calling Google Calendar API ...");

        setContentView(activityLayout);

        // Initialize credentials and service object.
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));

        // Build a new GoogleApiClient for the Wearable API
        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private DataMap createDataMap(){
        DataMap dataMap = new DataMap();
        if(eventTitles==null) {
            eventTitles = new ArrayList<>();
            eventTitles.add("yup");
        }
        if(eventTimes==null)
        {
            eventTimes = new long[1];
            eventTimes[0]=7;
        }
        if(eventTitles==null)
        {
            eventTitles = new ArrayList<>();
            eventTitles.add("doh");
        }
        if(eventTimesStrings==null)
        {
            eventTimesStrings = new ArrayList<>();
        }
        dataMap.putStringArrayList("eventTitles", eventTitles);

        //dataMap.putLongArray("eventTimes", eventTimes);

        dataMap.putStringArrayList("eventPhoneNumbers", eventPhoneNumbers);

        return dataMap;
    }
    private void sendDataMapToDataLayer()
    {
        if(googleClient.isConnected()) {
            for (int a = 0; a < eventTitles.size(); a++) {
                Log.v("DML", eventTitles.get(a));
                Log.v("DML2", eventTimesStrings.get(a));
            }
                DataMap dataMap = createDataMap();
                dataMap.putStringArrayList("eventTitles",eventTitles);
                dataMap.putStringArrayList("eventPhoneNumbers",eventPhoneNumbers);
                dataMap.putStringArrayList("eventTimes",eventTimesStrings);
                new SendDataMapToDataLayer(googleClient, WEARABLE_DATA_PATH, dataMap).start();
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        googleClient.connect();
    }

    /**
     * Called whenever this activity is pushed to the foreground, such as after
     * a call to onCreate().
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            refreshResults();
        } else {
            mOutputText.setText("Google Play Services required: " +
                    "after installing, close and relaunch this app.");
        }
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode code indicating the result of the incoming
     *     activity result.
     * @param data Intent (containing result data) returned by incoming
     *     activity result.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    mOutputText.setText("Account unspecified.");
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Attempt to get a set of data from the Google Calendar API to display. If the
     * email address isn't known yet, then call chooseAccount() method so the
     * user can pick an account.
     */
    private void refreshResults() {
        if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            if (isDeviceOnline()) {
                new MakeRequestTask(mCredential).execute();
            } else {
                mOutputText.setText("No network connection available.");
            }
        }
    }

    /**
     * Starts an activity in Google Play Services so the user can pick an
     * account.
     */
    private void chooseAccount() {
        startActivityForResult(
                mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date. Will
     * launch an error dialog for the user to update Google Play Services if
     * possible.
     * @return true if Google Play Services is available and up to
     *     date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS ) {
            return false;
        }
        return true;
    }

    @Override
    protected void onStop()
    {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        super.onStop();
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                MainActivity.this,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        //TODO

    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    /**
     * An asynchronous task that handles the Google Calendar API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, ArrayList<String>>
    {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential)
        {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call Google Calendar API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected ArrayList<String> doInBackground(Void... params)
        {
            try
            {
                return getDataFromApi();
            } catch (Exception e)
            {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         *
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private ArrayList<String> getDataFromApi() throws IOException
        {
            // List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            ArrayList<String> eventStrings = new ArrayList<>();

            ArrayList<Long> eventTimesArrayList = new ArrayList<>();

            eventTitles = new ArrayList<>();

            eventPhoneNumbers = new ArrayList<>();

            eventTimesStrings = new ArrayList<>();

            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();

            // Loop through the events fromsuccessfully sent to Google Calendar's API,
            // and put them into ArrayLists for sending to the
            // Wearable device
            for (Event event : items)
            {
                DateTime start = event.getStart().getDateTime();
                if (start == null)
                {
                    // All-day events don't have start times, so just use
                    // the start date.
                    start = event.getStart().getDate();
                }
                if(start == null)

                {

                    start = new DateTime(System.currentTimeMillis());

                }

                String eventTitle = event.getSummary();

                String eventPhoneNumber = event.getDescription();


                if(eventTitle==null)

                {

                    eventTitle="Event";

                }

                if(eventPhoneNumber ==null)

                {

                    eventPhoneNumber = "noPhoneNumberProvided";

                }

                eventTimesArrayList.add(start.getValue());

                eventTimesStrings.add(start.getValue()+"");

                eventTitles.add(eventTitle);

                eventPhoneNumbers.add(eventPhoneNumber);

                eventStrings.add(
                        // This has all the event data.  Just get whatever data is necessary,
                        // for instance, the phone number is stored in the
                        // "add note" section for the Google Calender event
                        String.format("%s (%s)", event.getSummary() + event.getDescription(), start));
            }

            eventTimes = new long[eventTimesArrayList.size()];

            for (int index = 0; index < eventTimesArrayList.size(); index++) {

                eventTimes[index] = eventTimesArrayList.get(index);

            }
            return eventStrings;
        }


        @Override
        protected void onPreExecute()
        {
            mOutputText.setText("");
            mProgress.show();
        }

        @Override
        protected void onPostExecute(ArrayList<String> output)
        {
            mProgress.hide();
            if (output == null || output.size() == 0)
            {
                mOutputText.setText("No results returned.");
            } else
            {

                // This is where we create the list based on the data received
                // from Google Calendar's API.
                // So, we need to create a WearableListView for each item returned.
                output.add(0, "Data retrieved using the Google Calendar API:");
                mOutputText.setText(TextUtils.join("\n", output));
            }
        }

        @Override
        protected void onCancelled()
        {
            mProgress.hide();
            if (mLastError != null)
            {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException)
                {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException)
                {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else
                {
                    mOutputText.setText("The following error occurred:\n"
                            + mLastError.getMessage());
                }
            } else
            {
                mOutputText.setText("Request cancelled.");
            }
        }
    }
}

