package com.example.calendarquickstart;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemAsset;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.Map;

// This class is in charge of sending/synchronizing the data to the Watch
//  Particularly, in regards to the calendar events and times.
public class SendDataMapToDataLayer extends  Thread {
    String path;
    GoogleApiClient googleApiClient;
    DataMap dataMap;
    public static final String WEARABLE_DATA_PATH = "/wearable/data/path";
    public static final String TAG = "TAG";

    public SendDataMapToDataLayer(GoogleApiClient googleApiClient, String path, DataMap dataMap) {
        // We store the parameters of the constructor, in local variables
        this.path = path;
        this.dataMap = dataMap;
        this.googleApiClient = googleApiClient;
    }

    @Override
    public void run() {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(WEARABLE_DATA_PATH);
        putDataMapRequest.getDataMap().putDataMap("dataMap",dataMap);
        PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest().setUrgent();
        DataApi.DataItemResult dataItemResult = Wearable.DataApi.putDataItem(googleApiClient,putDataRequest).await();
        if (dataItemResult.getStatus().isSuccess()) {
            //print success log
            Log.v(TAG, "###########DataItem: successfully sent#####################");
        } else {
            // print failure log
            Log.v(TAG, "Error while sending the DataItem");
        }
    }
}