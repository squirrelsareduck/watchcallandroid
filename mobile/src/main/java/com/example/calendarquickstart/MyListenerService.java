package com.example.calendarquickstart;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Andrew Elek
 */
public class MyListenerService extends WearableListenerService {

    public static final String TAG = "TAG";

    //TODO: make sure the paths don't interfere with each other, for each direction of communication
    public static final String WEARABLE_DATA_PATH = "/wearable/data/path";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equalsIgnoreCase(WEARABLE_DATA_PATH)) {
            final String message = new String(messageEvent.getData());
            Log.e(TAG, "PHONE CALL " + message);

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("tel:" + message));
            startActivity(intent);
        }
    }
}
